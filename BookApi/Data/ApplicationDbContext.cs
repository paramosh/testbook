﻿using BookApi.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookApi.Data
{
	public class ApplicationDbContext : DbContext
	{
		public ApplicationDbContext() : base("name=DefaultConnection")
		{
		}

		public virtual NoDeleteDbSet<Book> Books { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
		}
	}
}
