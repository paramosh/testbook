﻿using BookApi.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Threading;

namespace BookApi.Data
{
	public class NoDeleteDbSet<TEntity> : DbSet<TEntity> where TEntity: BaseEntity
	{
		public override TEntity Add(TEntity entity)
		{
			entity.CreatedOnUtc = DateTime.UtcNow;
			Update(entity);

			return base.Add(entity);
		}

		public override IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
		{
			var utcNow = DateTime.UtcNow;
			foreach (var entity in entities)
				entity.CreatedOnUtc = utcNow;
			UpdateRange(entities);

			return base.AddRange(entities);
		}

		public virtual void Update(TEntity entity)
		{
			entity.UpdatedOnUtc = DateTime.UtcNow;
		}

		public virtual void UpdateRange(IEnumerable<TEntity> entities)
		{
			var utcNow = DateTime.UtcNow;
			foreach (var entity in entities)
				entity.UpdatedOnUtc = utcNow;
		}

		public override TEntity Remove(TEntity entity)
		{
			entity.IsDeleted = true;
			Update(entity);

			return entity;
		}

		public override IEnumerable<TEntity> RemoveRange(IEnumerable<TEntity> entities)
		{
			foreach (var entity in entities)
				entity.IsDeleted = true;
			UpdateRange(entities);

			return entities;
		}
	}
}
