﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookApi.Entities
{
	public class Book : BaseEntity
	{
		public string Name { get; set; }
		public int NumberOfPages { get; set; }
		public DateTime DateOfPublication { get; set; }
		public string Authors { get; set; }
	}
}
