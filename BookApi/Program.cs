﻿using Microsoft.Owin.Hosting;
using System;
using System.Net.Http;

namespace BookApi
{
	class Program
	{
		static void Main(string[] args)
		{ 
			string baseAddress = "http://localhost:10281/";

			Console.WriteLine("Starting web Server...");

			// Start OWIN host.
			WebApp.Start<Startup>(baseAddress);

			Console.WriteLine($"Server running at {baseAddress} - press Enter to quit.");
			Console.ReadLine();
		}
	}
}
