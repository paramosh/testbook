﻿using BookApi.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookApi.Models
{
	public class BookModel
	{
		[Key]
		public Guid? Id { get; set; }

		[Required]
		[StringLength(60, MinimumLength = 3)]
		public string Name { get; set; }

		[Required]
		[Display(Name = "Number of pages")]
		public int? NumberOfPages { get; set; }

		[Required]
		[DataType(DataType.Date)]
		[Display(Name = "Date of publication")]
		public DateTime? DateOfPublication { get; set; }

		[Required]
		public string[] Authors { get; set; }
	}


	/// <summary>
	/// Extension for Book
	/// </summary>
	public static partial class BookExtension
	{
		/// <summary>
		/// Convert Book to BookModel 
		/// </summary>
		/// <param name="book"></param>
		/// <returns></returns>
		public static BookModel ToModel(this Book book)
		{
			return new BookModel
			{
				Id = book.Id,
				Name = book.Name,
				NumberOfPages = book.NumberOfPages,
				DateOfPublication = book.DateOfPublication,
				Authors = (book.Authors != null) ? book.Authors.Split(';') : null
			};
		}

		/// <summary>
		/// Update data in Book from BookModel (without Id)
		/// </summary>
		/// <param name="book"></param>
		/// <param name="model"></param>
		public static void UpdateFromModel(this Book book, BookModel model)
		{
			if (!String.IsNullOrWhiteSpace(model.Name))
				book.Name = model.Name;

			if (model.NumberOfPages.HasValue)
				book.NumberOfPages = model.NumberOfPages.Value;

			if (model.DateOfPublication.HasValue)
				book.DateOfPublication = model.DateOfPublication.Value;

			// Convert string[] to string. Required for proper storage in db
			if (model.Authors != null)
			{
				var strBuilder = new StringBuilder();

				foreach (var author in model.Authors)
					if (!String.IsNullOrWhiteSpace(author))
						strBuilder.Append(author + ';');

				// Remove last character ';'
				strBuilder.Remove(strBuilder.Length - 1, 1);

				book.Authors = strBuilder.ToString();
			}
		}
	}
}
