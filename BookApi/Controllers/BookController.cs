﻿using BookApi.Data;
using BookApi.Entities;
using BookApi.Models;
using Microsoft.Owin.Logging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace BookApi.Controllers
{
	public class BookController : ApiController
	{
		private readonly ApplicationDbContext _context;

		/// <summary>
		/// Default constructor
		/// </summary>
		public BookController()
		{
			this._context = new ApplicationDbContext();
		}

		public BookController(ApplicationDbContext context)
		{
			this._context = context;
		}


		/// <summary>
		/// Get books
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		[Route("api/books")]
		public async Task<IEnumerable<BookModel>> GetBooksAsync()
		{
			// Get all books
			var books = await _context.Books.Where(x => !x.IsDeleted).ToListAsync();

			// Convert Book to BookModel
			return books.Select(x => x.ToModel());
		}

		/// <summary>
		/// Get book by id
		/// </summary>
		/// <param name="id">Id of book</param>
		/// <returns></returns>
		[HttpGet]
		public BookModel GetBook(Guid id)
		{
			var book = _context.Books.Find(id);
			if (book == null || book.IsDeleted)
				return null;

			return book.ToModel();
		}

		/// <summary>
		/// Delete book by id
		/// </summary>
		/// <param name="id">Id of book</param>
		[HttpDelete]
		public IHttpActionResult DeleteBook(Guid id)
		{
			var book = _context.Books.Find(id);
			if (book == null || book.IsDeleted)
				return NotFound();

			_context.Books.Remove(book);
			_context.SaveChanges();

			return Ok();
		}

		/// <summary>
		/// Create book
		/// </summary>
		/// <param name="model">BookModel</param>
		[HttpPost]
		public IHttpActionResult CreateBook(BookModel model)
		{		
			if (model == null || !ModelState.IsValid)
				return NotFound();

			// Create new book.
			var book = new Book();
			book.UpdateFromModel(model);

			_context.Books.Add(book);
			_context.SaveChanges();
			return Ok();		
		}

		/// <summary>
		/// Update book
		/// </summary>
		/// <param name="model">BookModel</param>
		[HttpPatch]
		public IHttpActionResult UpdateBook(BookModel model)
		{
			if (model != null && model.Id.HasValue)
			{
				var book = _context.Books.Find(model.Id.Value);

				if (book != null && !book.IsDeleted)
				{
					book.UpdateFromModel(model);

					_context.Books.Update(book);
					_context.SaveChanges();
					return Ok();
				}
			}

			return NotFound();
		}
	}
}

