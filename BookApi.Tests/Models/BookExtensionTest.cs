﻿using BookApi.Entities;
using BookApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookApi.Tests.Models
{
	[TestClass]
	public class BookExtensionTest
	{
		[TestMethod]
		public void TestToModel()
		{
			var book = GetBook();
			var bookModel = GetBookModel();

			// Act
			var bookToModel = book.ToModel();

			// Assert
			Assert.AreEqual(bookModel.Id, bookToModel.Id);
			Assert.AreEqual(bookModel.Name, bookToModel.Name);
			Assert.AreEqual(bookModel.NumberOfPages, bookToModel.NumberOfPages);
			Assert.AreEqual(bookModel.Authors.Count(), bookToModel.Authors.Count());
			Assert.AreEqual(bookModel.DateOfPublication, bookToModel.DateOfPublication);
		}

		[TestMethod]
		public void TestToModelAuthors()
		{
			var book = GetBook();
			book.Authors = "author1;author2;author3;Big Author '1'";
			var expectedAuthors = new string[]
			{
				"author1",
				"author2",
				"author3",
				"Big Author '1'"
			};

			// Act
			var authors = book.ToModel().Authors;

			// Assert
			for (int i = 0; i < authors.Length; i++)
				Assert.AreEqual(expectedAuthors[i], authors[i]);
		}

		[TestMethod]
		public void TestUpdateFromModelAuthors()
		{
			var book = new Book();
			var bookModel = GetBookModel();
			var expectedAuthors = "author1;author2;author3;Big Author '1'";
			bookModel.Authors = new string[]
			{
				"author1",
				"author2",
				"author3",
				"Big Author '1'"
			};

			// Act 1
			book.UpdateFromModel(bookModel);

			// Assert 1
			Assert.AreEqual(expectedAuthors, book.Authors);


			expectedAuthors = "author1;author3;Big Author '1'";
			bookModel.Authors = new string[]
			{
				"author1",
				null,
				"author3",
				"",
				"Big Author '1'"
			};

			// Act 2
			book.UpdateFromModel(bookModel);

			// Assert 2
			Assert.AreEqual(expectedAuthors, book.Authors);
		}

		#region Get testing data

		private BookModel GetBookModel()
		{
			return new BookModel
			{
				Id = Guid.Parse("82350f30-398f-41f3-a82b-5279fa404b17"),
				Authors = new string[]
				{
					"author7",
					"Custom Author 33",
					"Author#4"
				},
				DateOfPublication = new DateTime(2017, 1, 1),
				Name = "Alone Book",
				NumberOfPages = 150
			};
		}

		private Book GetBook()
		{
			return new Book
			{
				Id = Guid.Parse("82350f30-398f-41f3-a82b-5279fa404b17"),
				Authors = "author7;Custom Author 33;Author#4",
				DateOfPublication = new DateTime(2017, 1, 1),
				Name = "Alone Book",
				NumberOfPages = 150
			};
		}

		#endregion
	}
}
