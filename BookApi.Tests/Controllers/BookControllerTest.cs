﻿using BookApi.Controllers;
using BookApi.Data;
using BookApi.Entities;
using BookApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BookApi.Tests.Controllers
{
	[TestClass]
	public class BookControllerTest
	{
		private BookController _controller;
		private Mock<NoDeleteDbSet<Book>> _mockSet;
		private Mock<ApplicationDbContext> _mockContext;


		[TestMethod]
		public void TestGetBook()
		{
			InitProperties();

			// Act
			var id = Guid.Parse("273ceeaa-681f-452b-b0c3-6ec608f3f32b");
			var book = _controller.GetBook(id);

			// Assert
			_mockSet.Verify(m => m.Find(It.IsAny<Guid>()), Times.Once());
		}

		[TestMethod]
		public void TestRemoveBook()
		{
			InitProperties();
			
			// Act
			var id = Guid.Parse("273ceeaa-681f-452b-b0c3-6ec608f3f32b");
			var book = _controller.DeleteBook(id);

			// Assert
			_mockSet.Verify(m => m.Remove(It.IsAny<Book>()), Times.Once());
			_mockContext.Verify(m => m.SaveChanges(), Times.Once());
		}

		[TestMethod]
		public void TestCreateBook()
		{
			InitProperties();

			var bookModel = GetOneBookModel();
			var book = new Book();
			book.UpdateFromModel(bookModel);

			// Act
			_controller.CreateBook(bookModel);

			// Assert
			_mockSet.Verify(m => m.Add(It.IsAny<Book>()), Times.Once());
			_mockContext.Verify(m => m.SaveChanges(), Times.Once());
		}

		[TestMethod]
		public void TestUpdateBook()
		{
			InitProperties();

			// Act
			var model1 = GetBookModel1();
			_controller.UpdateBook(model1);

			// Assert
			_mockSet.Verify(m => m.Update(It.IsAny<Book>()), Times.Once());
			_mockContext.Verify(m => m.SaveChanges(), Times.Once());
		}

		#region 

		private void InitProperties()
		{
			// Put some data to db.
			var books = GetFiveBooks();
			books.AddRange(GetThreeDeletedBooks());
			var data = books.AsQueryable();

			// Create mock DbSet.
			_mockSet = new Mock<NoDeleteDbSet<Book>>();
			_mockSet.As<IQueryable<Book>>().Setup(m => m.Provider).Returns(data.Provider);
			_mockSet.As<IQueryable<Book>>().Setup(m => m.Expression).Returns(data.Expression);
			_mockSet.As<IQueryable<Book>>().Setup(m => m.ElementType).Returns(data.ElementType);
			_mockSet.As<IQueryable<Book>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());
            _mockSet.Setup(m => m.Find(It.IsAny<object>())).Returns((object[] a) => data.FirstOrDefault(x => x.Id == (Guid)a[0]));

            // Create mock DbContext.
            _mockContext = new Mock<ApplicationDbContext>();
			_mockContext.Setup(m => m.Books).Returns(_mockSet.Object);

			// Create controller.
			_controller = new BookController(_mockContext.Object);
		}


		#endregion


		#region Testing data

		public List<Book> GetFiveBooks()
		{
			return new List<Book>
			{
				new Book
				{
					Id = Guid.Parse("57c7c3de-3869-448a-b53d-596a5c1acf33"),
					Authors = "author1;author2;author3",
					DateOfPublication = new DateTime(2017, 10, 10),
					Name = "Book1",
					NumberOfPages = 100
				},
				new Book
				{
					Id = Guid.Parse("94933478-201d-45e8-a078-ece14dd9f268"),
					Authors = "author3;author4",
					DateOfPublication = new DateTime(2011, 11, 11),
					Name = "Book2",
					NumberOfPages = 200
				},
				new Book
				{
					Id = Guid.Parse("510108f0-2675-479f-b758-30c9d5182364"),
					Authors = "author5;author8;author1",
					DateOfPublication = new DateTime(1999, 01, 11),
					Name = "Book3",
					NumberOfPages = 300
				},
				new Book
				{
					Id = Guid.Parse("860bb230-6d03-46d3-8f26-75332492933d"),
					Authors = null,
					DateOfPublication = new DateTime(2011, 11, 11),
					Name = "Book4",
					NumberOfPages = 400
				},
				new Book
				{
					Id = Guid.Parse("273ceeaa-681f-452b-b0c3-6ec608f3f32b"),
					Authors = "author3",
					DateOfPublication = new DateTime(2017, 12, 18),
					Name = "Book5",
					NumberOfPages = 500
				}
			};
		}

		private List<Book> GetThreeDeletedBooks()
		{
			return new List<Book>
			{
				new Book
				{
					Id = Guid.Parse("d7758f2f-8f5a-456a-8253-faa4cb79c582"),
					Authors = "author1;author3",
					DateOfPublication = new DateTime(2017, 12, 07),
					Name = "Book1d",
					NumberOfPages = 1000,
					IsDeleted = true
				},
				new Book
				{
					Id = Guid.Parse("bc6b1580-93ba-4b37-8246-5ba7f684af5b"),
					Authors = null,
					DateOfPublication = new DateTime(2015, 11, 20),
					Name = "Book2d",
					NumberOfPages = 2000,
					IsDeleted = true
				},
				new Book
				{
					Id = Guid.Parse("fabc0933-34dc-4418-915f-b02251bcd340"),
					Authors = "author7;author9;author1",
					DateOfPublication = new DateTime(2016, 5, 25),
					Name = "Book3d",
					NumberOfPages = 3000,
					IsDeleted = true
				}
			};
		}

		private BookModel GetOneBookModel()
		{
			return new BookModel
			{
				Id = Guid.Parse("82350f30-398f-41f3-a82b-5279fa404b17"),
				Authors = new string[] 
				{
					"author7",
					"Custom Author 33",
					"Author#4"
				},
				DateOfPublication = new DateTime(2017, 1, 1),
				Name = "Alone Book",
				NumberOfPages = 150
			};
		}

		private BookModel GetBookModel1()
		{
			return new BookModel
			{
				Id = Guid.Parse("57c7c3de-3869-448a-b53d-596a5c1acf33"),
				Name = "Updated Book",
				NumberOfPages = 250
			};
		}

		private BookModel GetBookModel2()
		{
			return new BookModel
			{
				Id = Guid.Parse("94933478-201d-45e8-a078-ece14dd9f268"),
				Authors = new string[]
				{
					"author1",
					"author2",
					"author3"
				},
				DateOfPublication = new DateTime(1999, 09, 09),
			};
		}
		#endregion
	}
}
